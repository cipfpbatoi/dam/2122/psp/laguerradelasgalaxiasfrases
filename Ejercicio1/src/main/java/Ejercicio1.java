import java.io.IOException;

public class Ejercicio1 {

    public static void main(String[] args) {

        ProcessBuilder processBuilder = new ProcessBuilder("java", "-jar", "UnaNuevaEsperanza.jar","Pepe");

        //Con inheritio, se mostrará la salida tanto si hay error como si no.
        processBuilder.inheritIO();

        try {
            Process p = processBuilder.start();

            p.waitFor();

        } catch (IOException e) {
            System.err.println("Hubo un problema lanzando el proceso " + e.getLocalizedMessage());
        } catch (InterruptedException e) {
            System.err.println("Se ha interrumpido el proceso hijo " + e.getLocalizedMessage());
        }

    }

}
