import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Ejercicio4 {

    public static void main(String[] args) {

        ArrayList<String> comando = new ArrayList<>();

        comando.add("java");
        comando.add("-jar");
        comando.add("UnaNuevaEsperanza.jar");
        comando.add("-i");

        ProcessBuilder pb = new ProcessBuilder(comando);

        pb.redirectErrorStream(true);

        try (Scanner sc = new Scanner(new FileInputStream("entrada.txt"));
            PrintWriter pw = new PrintWriter(new FileOutputStream("salida.txt"),true)){

            Process ej2process = pb.start();

            Scanner childReader = new Scanner(ej2process.getInputStream());
            PrintWriter printWriter = new PrintWriter(ej2process.getOutputStream(), true);



            while (sc.hasNextLine()) {

                printWriter.println(sc.nextLine());

                //Puede ocurrir que falle cuando se le pasa el stop, se cierra el canal
                if (childReader.hasNextLine()){
                    String leido = childReader.nextLine();
                    System.out.println(leido);
                    pw.println(leido);
                }

            }

        } catch (IOException e) {
            System.out.println("IOException " + e.getLocalizedMessage());
        }


    }
}
