# La Guerra de las Galaxias Quotes

Se ha desarrollado un jar que puedes descargar desde aquí el fuente o desde aules el jar 
y programa que imprime frases aleatorias de los personajes de la saga que le indiquemos.

La documentación de como utilizar el programa se encuetra en [Una Nueva Esperanza](UnaNuevaEsperanza)

Realiza los siguientes ejercicios:

## Ejercicio 1 (1p)

Realiza la clase Ejercicio1 ejecuta el proceso hijo en modo comando
pasándole como único argumento Leia. Utiliza para ellos los métodos de la
clase Process que nos permitirían mostrar el resultado de la ejecución del comando tanto si se ha ejecutado 
correctamente como si ha habido algún problema

## Ejercicio 2 (2p)

Realiza la clase Ejercicio2 que ejecute el proceso hijo en modo interactivo (sin argumentos extra) y que reenvíe al usuario todas 
las entradas y salidas del proceso hijo mediante métodos de la clase ProcessBuilder 

Realiza la clase Ejercicio2Rebels para rebeldes que realiza esto mismo, pero utilizando la redirección de procesos, para ello,
el proceso padre debe de mostrar las cadenas cuando le pide al usuario valores: 
* "Introduce el Nombre del Personaje:"
* "La frase del personaje es:"

## Ejercicio 3 (3p)

Realiza la clase Ejercicio4 que acepte como parámetros una lista de nombres y opcionalmente, una lista de frases propias 
mediante \[-d frases.js\] por simplicidad, el fichero de frases, en caso de estar irá al principio. 

La clase Ejercicio4 ha de ejecutar el proceso hijo por cada valor en modo comando pasado como entrada 
y mostrar una lista con los resultados por la salida:

Ejemplo de ejecución: java -jar Ejercicio4.jar -d diccionario.txt Leia Luke

( No es absolutamente necesario generar el jar para la entrega)

* Ayúdame Obi-Wan Kenobi... Eres mi única esperanza...
* desconocido

## Ejercicio 4 (4p)

En la clase Ejercicio5 se tiene que realizar un programa que lea todos los nombres contenidos en entrada.txt (Uno por 
línea), que ejecute el hijo en modo interactivo(-i) le pase todos los valores, imprima todos los resultados por pantalla
y guarde el resultado en un fichero.





