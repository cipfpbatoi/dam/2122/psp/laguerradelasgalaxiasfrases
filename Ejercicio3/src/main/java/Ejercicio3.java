import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Ejercicio3 {

    public static void main(String[] args){

    ArrayList<String> comando = new ArrayList<>();

        comando.add("java");
        comando.add("-jar");
        comando.add("UnaNuevaEsperanza.jar");

        int indiceParametrosPersonajes = 0;

        if (args.length > 2 && "-d".equalsIgnoreCase(args[0])){
            comando.add("-d");
            comando.add(args[1]);
            indiceParametrosPersonajes = 2;
        }

        for (int i = indiceParametrosPersonajes; i < args.length; i++){

            ArrayList<String> parametrosSubproceso = new ArrayList<>(comando);
            parametrosSubproceso.add(args[i]);

            ProcessBuilder pb = new ProcessBuilder(parametrosSubproceso);

            try {

                Process ej2process = pb.start();

                Scanner childReader = new Scanner(ej2process.getInputStream());

                int resultado = ej2process.waitFor();

                if ( resultado == 0){
                    System.out.println(childReader.nextLine());
                } else {
                    System.err.println("desconocido");
                }


            } catch (
                    IOException e) {
                System.out.println("IOException " + e.getLocalizedMessage());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
