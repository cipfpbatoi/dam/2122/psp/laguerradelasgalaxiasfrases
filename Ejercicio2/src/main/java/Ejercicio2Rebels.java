import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Ejercicio2Rebels {

    public static void main(String[] args) {

        ArrayList<String> comando = new ArrayList<>();

        comando.add("java");
        comando.add("-jar");
        comando.add("UnaNuevaEsperanza.jar");
        comando.add("-i");

        ProcessBuilder pb = new ProcessBuilder(comando);

        pb.redirectErrorStream(true);

        String frasePedir = "Introduce el Nombre del Personaje: ";
        String fraseResultado = "La frase del personaje es: ";

        try {

            Process ej2process = pb.start();

            Scanner childReader = new Scanner(ej2process.getInputStream());
            PrintWriter printWriter = new PrintWriter(ej2process.getOutputStream(),true);

            Scanner sc = new Scanner(System.in);

            String entradaTeclado = "";
            System.out.print(frasePedir);
            while ( !(entradaTeclado = sc.nextLine()).equalsIgnoreCase("stop")){
                printWriter.println(entradaTeclado);
                System.out.print(fraseResultado); System.out.println(childReader.nextLine());
                System.out.print(frasePedir);
            }


        } catch (IOException e) {
            System.out.println("IOException " + e.getLocalizedMessage());
        }

    }


}
