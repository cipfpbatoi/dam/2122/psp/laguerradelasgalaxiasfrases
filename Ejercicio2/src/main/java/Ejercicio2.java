import java.io.IOException;
import java.util.ArrayList;

public class Ejercicio2 {

    public static void main(String[] args) {

        ArrayList<String> comando = new ArrayList<>();

        comando.add("java");
        comando.add("-jar");
        comando.add("UnaNuevaEsperanza.jar");
        comando.add("-i");

        ProcessBuilder pb = new ProcessBuilder(comando);

        pb.inheritIO();

        try {
            Process ej2process = pb.start();
            ej2process.waitFor();

        } catch (IOException e) {
            System.out.println("IOException " + e.getLocalizedMessage());
        } catch (InterruptedException e) {
            System.out.println("InterruptedException " + e.getLocalizedMessage());
        }

    }

}
